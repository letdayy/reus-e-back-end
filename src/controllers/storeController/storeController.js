const { response } = require('express');
const store = require('../../models/store');
const product = require('../../models/product');


const create = async(req, res) => {
    try {
        const Store = await store.create(req.body);
        return res.status(201).json({message: "Loja cadastrada com sucesso.", store: Store});
    }
    catch(err) {
        res.status(500).json(`${err}!`);
    }
};

const index = async(req, res) => {
    try {
        const Store = await store.findAll();
        return res.status(200).json(Store);
    }
    catch(err) {
        res.status(500).json({error: err});
    }
};

const show = async(req, res) => {
    const {id} = req.params;
    try {
        const Store = await store.findByPk(id);
        return res.status(200).json(Store);
    }
    catch(err) {
        res.status(500).json(`${err}!`);
    }
};

const update = async(req, res) => {
    const {id} = req.params;
    try {
        const [update] = await store.update(req.body, {where: {id: id}});
        if(update) {
            const Store = await store.findByPk(id);
            return res.status(200).send(Store);
        }
        throw new Error();
    }
    catch(err) {
        return res.status(500).json("Loja não encontrada.");
    }
};

const destroy = async(req,res) => {
    const{id} = req.params;
    try {
        const deleted = await store.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Loja deletada com sucesso.");
        }
        throw new Error();
    }
    catch(err) {
        return res.status(500).json("Loja não encontrada.");
    }
};

module.exports = {
    index,
    show,
    create,
    update,
    destroy
};