const { response } = require('express');
const user = require('../../models/user');
const product = require('../../models/product');

const create = async(req, res) => {
    try {
        const User = await user.create(req.body);
        return res.status(201).json({message: "Usuário cadastrado com sucesso.", user: User});
    }
    catch(err) {
        res.status(500).json(`${err}!`);
    }
};

const index = async(req, res) => {
    try {
        const User = await user.findAll();
        return res.status(200).json(User);
    }
    catch(err) {
        console.log(err);
        res.status(500).json({error: err});
    }
};

const show = async(req, res) => {
    const {id} = req.params;
    try {
        const User = await user.findByPk(id);
        return res.status(200).json(User);
    }
    catch(err) {
        console.log(err);
        res.status(500).json(`${err}!`);
    }
};

const update = async(req, res) => {
    const {id} = req.params;
    try {
        const [update] = await user.update(req.body, {where: {id: id}});
        if(update) {
            const User = await user.findByPk(id);
            return res.status(200).send(User);
        }
        throw new Error();
    }
    catch(err) {
        return res.status(500).json("Usuário não encontrado.");
    }
};

const destroy = async(req,res) => {
    const{id} = req.params;
    try {
        const deleted = await user.destroy({where: {id: id}});
        if(deleted) {
            return res.status(200).json("Usuário deletado com sucesso.");
        }
        throw new Error();
    }
    catch(err) {
        return res.status(500).json("Usuário não encontrado.");
    }
};

const addProduct = async(req,res) => {
    const {id} = req.params;
    try {
        const Product = await product.findByPk(id);
        const User = await user.findByPk(req.body.userId);
        await Product.setUser(User);
        return res.status(200).json(Product);
    } catch(err) {
        console.log(err);
        return res.status(500).json({err});
    }
};

const removeProduct = async(req,res) => {
    const {id} = req.params;
    try {
        const Product = await product.findByPk(id);
        await Product.setUser(null);
        return res.status(200).json(Product);
    } catch(err) {
        console.log(err);
        return res.status(500).json({err});
    }
};

module.exports = {
    index,
    show,
    create,
    update,
    destroy,
    addProduct,
    removeProduct
};