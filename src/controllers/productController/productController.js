const { response } = require('express');
const product = require('../../models/product');
const user = require('../../models/user');
const store = require('../../models/store');

const create = async(req, res) => {
    try {
        const Product = await product.create(req.body);
        return res.status(201).json({message: "Produto adicionado com sucesso.", product: Product});
    }
    catch(err) {
        res.status(500).json(`${err}!`);
    }
};

const index = async(req, res) => {
    try {
        const Product = await product.findAll();
        return res.status(200).json(Product);
    }
    catch(err) {
        res.status(500).json({error: err});
    }
};

const show = async(req, res) => {
    const {id} = req.params;
    try {
        const Product = await product.findByPk(id);
        return res.status(200).json(Product);
    }
    catch(err) {
        res.status(500).json(`${err}!`);
    }
};

const update = async(req, res) => {
    const {id} = req.params;
    try {
        const [update] = await product.update(req.body, {where: {id: id}});
        if(update) {
            const Product = await product.findByPk(id);
            return res.status(200).send(Product);
        }
        throw new Error();
    }
    catch(err) {
        console.log(err);
        return res.status(500).json("Produto não encontrado.");
    }
};

const destroy = async(req,res) => {
    const{id} = req.params;
    try {
        const Deleted = await product.destroy({where: {id: id}});
        if(Deleted) {
            return res.status(200).json("Produto removido com sucesso.");
        }
        throw new Error();
    }
    catch(err) {
        return res.status(500).json("Produto não encontrado.");
    }
};

const addProduct = async(req,res) => {
    const {id} = req.params;
    try {
        const Product = await product.findByPk(id);
        const Store = await store.findByPk(req.body.storeId);
        await Product.setStore(Store);
        return res.status(200).json(Product);
    } catch(err) {
        return res.status(500).json({err});
    }
};

const removeProduct = async(req,res) => {
    const {id} = req.params;
    try {
        const Product = await product.findByPk(id);
        await Product.setStore(null);
        return res.status(200).json(Product);
    } catch(err) {
        return res.status(500).json({err});
    }
};

module.exports = {
    index,
    show,
    create,
    update,
    destroy,
    addProduct,
    removeProduct
};

