const { Router } = require('express');
const userController = require('../controllers/userController/usercontroller');
const productController = require('../controllers/productController/productController');
const storeController = require('../controllers/storeController/storeController');
const router = Router();

//routes for user
router.get('/users', userController.index);
router.get('/users/:id',userController.show);
router.post('/users',userController.create);
router.put('/users/:id',userController.update);
router.delete('/users/:id',userController.destroy);
router.put('/users/addproduct/:id',userController.addProduct);
router.delete('/users/removeproduct/:id',userController.removeProduct);

//routes for product
router.get('/product', productController.index);
router.get('/product/:id',productController.show);
router.post('/product',productController.create);
router.put('/product/:id',productController.update);
router.delete('/product/:id',productController.destroy);
router.put('/productaddrole/:id',productController.addProduct);
router.delete('/productremoverole/:id',productController.removeProduct);


//routes for store
router.get('/store', storeController.index);
router.get('/store/:id',storeController.show);
router.post('/store',storeController.create);
router.put('/store/:id',storeController.update);
router.delete('/store/:id',storeController.destroy);

module.exports = router;