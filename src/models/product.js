const DataTypes = require('sequelize');
const sequelize = require('../config/sequelize');

const product = sequelize.define('product', {
    name: {
        type: DataTypes.STRING,
        allowNull: false
    },
    photo: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    price: {
        type: DataTypes.DOUBLE,
        allowNull: false
    },
    category: {
        type: DataTypes.STRING,
        allowNull: false
    },
    description: {
        type: DataTypes.STRING,
        allowNull: false
    },
    paymentMethod: {
        type: DataTypes.STRING,
        allowNull: false
    },
});

product.associate = function(models) {
    product.belongsTo(models.user);
    product.belongsTo(models.store);
};

module.exports = product;