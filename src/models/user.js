const DataTypes = require('sequelize');
const sequelize = require('../config/sequelize');

const user = sequelize.define('user', {
    name: {
        type: DataTypes.STRING,
        allowNull: false
        },
    email: {
        type: DataTypes.STRING,
        allowNull: false
        },
    phone: {
        type: DataTypes.STRING,
        allowNull: false
        },
    adress: {
        type: DataTypes.STRING,
        allowNull: false
        },
    RG: {
        type: DataTypes.STRING,
        allowNull: false
        },
    CPF: {
        type: DataTypes.STRING,
        allowNull: false
        },
    birthday: {
        type: DataTypes.DATEONLY,
        allowNull: false
        },
    hash: {
        type: DataTypes.STRING,
        allowNull: false
        },
    salt: {
        type: DataTypes.STRING,
        allowNull: false
        },
    
});

user.associate = function(models) {
    user.hasMany(models.product);
};

module.exports = user;