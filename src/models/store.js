const DataTypes = require('sequelize');
const sequelize = require('../config/sequelize');

const store = sequelize.define('store', {
    name: {
        type: DataTypes.STRING,
        allowNull: false
        },
    phone: {
        type: DataTypes.STRING,
        allowNull: false
        },
    email: {
        type: DataTypes.STRING,
        allowNull: false
        },
});

store.associate = function(models) {
    store.hasMany(models.product);
};

module.exports = store;